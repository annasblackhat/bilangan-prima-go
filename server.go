package main

import (
	"encoding/json"
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"log"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	router := fasthttprouter.New()
	router.GET("/api/prima", func(ctx *fasthttp.RequestCtx) {
		var y int
		var a int
		var g int

		fmt.Print("Deretan Primanya-> ")
		for y=1; y<=10000; y++ {
			a = 0
			for g=1; g<=y; g++ {
				if y % g == 0 {
					a++
				}
			}

			if a == 2 {
				fmt.Println(y)
			}
		}

		ctx.SetContentType("application/json")
		_=json.NewEncoder(ctx).Encode(map[string]interface{}{
			"message": "bilangan prima berhasil di generate oleh Go",
			"status": true,
		})
	})
	log.Fatal(fasthttp.ListenAndServe(":" + port, router.Handler))
}
